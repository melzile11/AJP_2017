#!/bin/bash -u

actpar=++actpar++;                                  # active parameter string
CLMstr=++CLMstr++;                                  # command line modifier string
simnum=++simnum++;                                  # simulation number  
num_beats=74;                                       # number of beats for each bcl in bcl string (bcls)
num_beats_ss=1000;                                  # number of beats for steady state (bcl_ss)
bcls=($( seq 500 -50 300; seq 290 -10 200 ));       # bcl's used in dynamic pacing protocol
bcl_ss=750;                                         # bcl used for the steady state prepacing prior to dynamic pacing protocol  
#bcls=($( seq 650 -50 300 ))
#bcl_ss=1000;
dt_tstep_ss=1                                       # time step (ms) for steady state
dt_outfile_ss=1                                     # output file temporal output granularity (ms)for steady state
dt_tstep=0.01                                       # time step (ms)
dt_outfile=0.1                                      # output file temporal output granularity (ms)
 
scdir=/home/mzile/LibmeshMechanics/build;           # singleCell directory
savedir=++savedir++;                                # save directory
simdir=++simdir++;                                  # directory for a particular simulation

# prepacing (get steady state @ bcl=bcl_ss)
basename_ss=GrandiRice_bcl${bcl_ss}ss_sim${simnum};
echo "${scdir}/singleCell --active=Grandi_rice --passive=RicePassive --active-par=${actpar} --isometric ${CLMstr} --num-beats=${num_beats_ss} --dt=${dt_tstep_ss} --dt-out=${dt_outfile_ss} --bcl=${bcl_ss} --save-ini-file=${savedir}/${simdir}/${basename_ss}.sv  >| ${savedir}/${simdir}/${basename_ss}.dat 2>| ${savedir}/${simdir}/${basename_ss}_header"
echo $( ${scdir}/singleCell --active=Grandi_rice --passive=RicePassive --active-par=${actpar} --isometric ${CLMstr} --num-beats=${num_beats_ss} --dt=${dt_tstep_ss} --dt-out=${dt_outfile_ss} --bcl=${bcl_ss} --save-ini-file=${savedir}/${simdir}/${basename_ss}.sv  >| ${savedir}/${simdir}/${basename_ss}.dat 2>| ${savedir}/${simdir}/${basename_ss}_header )

# loop through bcl's in dynamic pacing protocol
basename_previous=${basename_ss};
for ii in $( seq 0 1 $(( ${#bcls[@]}-1 )) ); do
    
    echo ${bcls[ii]}
    basename=GrandiRice_bcl${bcls[ii]}_sim${simnum};
    echo "${scdir}/singleCell --active=Grandi_rice --passive=RicePassive --active-par=${actpar} --isometric ${CLMstr} --num-beats=${num_beats} --dt=${dt_tstep} --dt-out=${dt_outfile} --bcl=${bcls[ii]} --read-ini-file=${savedir}/${simdir}/${basename_previous}.sv --save-ini-file=${savedir}/${simdir}/${basename}.sv  >| ${savedir}/${simdir}/${basename}.dat 2>| ${savedir}/${simdir}/${basename}_header"
    echo $( ${scdir}/singleCell --active=Grandi_rice --passive=RicePassive --active-par=${actpar} --isometric ${CLMstr} --num-beats=${num_beats} --dt=${dt_tstep} --dt-out=${dt_outfile} --bcl=${bcls[ii]} --read-ini-file=${savedir}/${simdir}/${basename_previous}.sv --save-ini-file=${savedir}/${simdir}/${basename}.sv  >| ${savedir}/${simdir}/${basename}.dat 2>| ${savedir}/${simdir}/${basename}_header )

    basename_previous=${basename};
    echo
done
